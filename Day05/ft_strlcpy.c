//
// Created by kingh on 2018/08/29.
//
#include <stdlib.h>
#include <stdio.h>

size_t ft_strlen(char * str);

size_t ft_strlcpy(char *dest, const char *src, const size_t size) {
	size_t i;
	const char *src_cpy;

	i = size - 1;
	src_cpy = src;
	while (i-- != 0 ) {
		if((*dest++ = *src_cpy++) == '\0') {
			break;
		}
	}

	i = ft_strlen((char*)src);
	*dest = '\0';

	return (i);
}

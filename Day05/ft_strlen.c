//
// Created by kingh on 2018/06/07.
//
#include <stdlib.h>

size_t ft_strlen(char * str) {
    size_t i;

    i = 0;
    while(str[i] != '\0') {
        i++;
    }

    return i;
}
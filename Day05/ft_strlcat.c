//
// Created by kingh on 2018/06/08.
//
#include <stdlib.h>
size_t ft_strlen(char * str);

size_t ft_strlcat(char *dest, const char *src, size_t size) {
    size_t n;
    char *dest_ptr;
    size_t dest_len;
    const char *src_ptr;

    src_ptr = src;
    dest_ptr = dest;
    n = size;
    while(*dest_ptr != '\0' && n--) {
        dest_ptr++;
    }
    dest_len = dest_ptr - dest;
    n = size - dest_len;

    if(n == 0) {
        return (dest_len + ft_strlen((char*)src));
    }

    while(*src_ptr != '\0') {
        if(n != 1) {
            *dest_ptr++ = *src_ptr;
            n--;
        }
        src_ptr++;
    }
    *dest_ptr = '\0';

    return (dest_len + ((char*)src_ptr - src));
}


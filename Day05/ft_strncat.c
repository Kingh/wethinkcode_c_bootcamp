//
// Created by kingh on 2018/06/07.
//
#include <stdlib.h>

size_t ft_strlen(char *str);

char *ft_strncat(char *dest, char *src, size_t nb) {

    char *ptr;
    ptr = dest + ft_strlen(dest);

    while(*src != '\0' && nb--) {
        *ptr++ = *src++;
    }
    *ptr = '\0';

    return dest;
}